package de.pstorch.turingmachine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static de.pstorch.turingmachine.Direction.*;

public class TuringMachineTest {

    final DefaultStateListener listener = new DefaultStateListener();

    @Test
    public void testTuringsFirst() {
        new TuringMachine().startState("q1")
            .add("q1", ' ', '0', R, "q2")
            .add("q2", ' ', ' ', R, "q3")
            .add("q3", ' ', '1', R, "q4")
            .add("q4", ' ', ' ', R, "q1")
            .run(listener, 21);
        Assertions.assertEquals(listener.getIterations(), 21);
        Assertions.assertTrue(listener.isMaxIterationsReached());
        Assertions.assertEquals(String.valueOf(listener.getResult()), "0 1 0 1 0 1 0 1 0 1 0 ");
    }

    @Test
    public void testCopy() {
        new TuringMachine().startBand("11111").startState("s1").defaultSymbol('0')
            .add("s1", '0', null, N, "H")
            .add("s1", '1', '0', R, "s2")
            .add("s2", '0', '0', R, "s3")
            .add("s2", '1', '1', R, "s2")
            .add("s3", '0', '1', L, "s4")
            .add("s3", '1', '1', R, "s3")
            .add("s4", '0', '0', L, "s5")
            .add("s4", '1', '1', L, "s4")
            .add("s5", '0', '1', R, "s1")
            .add("s5", '1', '1', L, "s5")
            .run(listener);
        Assertions.assertEquals(String.valueOf(listener.getResult()), "11111011111");
    }

    @Test
    public void testBusyBeaver() {
        new TuringMachine().startBand("00000000000000").startState("A").defaultSymbol('0').startPos(7)
            .add("A", '0', '1', R, "B")
            .add("A", '1', '1', L, "C")
            .add("B", '0', '1', L, "A")
            .add("B", '1', '1', R, "B")
            .add("C", '0', '1', L, "B")
            .add("C", '1', '1', N, "H")
            .run(listener);
        Assertions.assertEquals(String.valueOf(listener.getResult()), "00001111110000");
    }

    @Test
    public void testBinaryCounter() {
        new TuringMachine().startBand("1011").startState("0").startPos(3)
            .add("0", ' ', ' ', L, "1")
            .add("0", '0', '0', R, "0")
            .add("0", '1', '1', R, "0")
            .add("1", ' ', '1', R, "2")
            .add("1", '0', '1', L, "2")
            .add("1", '1', '0', L, "1")
            .add("2", ' ', ' ', L, "H")
            .add("2", '0', '0', R, "2")
            .add("2", '1', '1', R, "2")
            .run(listener);
        Assertions.assertEquals(String.valueOf(listener.getResult()).trim(), "1100");
    }

    @Test
    public void testPalindromeDetectorTrue() {
        assertPalindrome("0110110", true);
    }

    @Test
    public void testPalindromeDetectorFalse() {
        assertPalindrome("110110", false);
    }

    private void assertPalindrome(final String input, final boolean isPalindrome) {
        new TuringMachine().startBand(input).startState("0")
            .add("0", ' ', ' ', R, "1")
            .add("0", '0', '0', L, "0")
            .add("0", '1', '1', L, "0")
            .add("1", ' ', '1', N, "H")
            .add("1", '0', ' ', R, "2")
            .add("1", '1', ' ', R, "4")
            .add("2", ' ', ' ', L, "3")
            .add("2", '0', '0', R, "2")
            .add("2", '1', '1', R, "2")
            .add("3", ' ', ' ', N, "0")
            .add("3", '0', ' ', L, "0")
            .add("3", '1', '1', N, "6")
            .add("4", ' ', ' ', L, "5")
            .add("4", '0', '0', R, "4")
            .add("4", '1', '1', R, "4")
            .add("5", ' ', ' ', N, "0")
            .add("5", '0', '0', N, "6")
            .add("5", '1', ' ', L, "0")
            .add("6", ' ', '0', N, "H")
            .add("6", '0', ' ', L, "6")
            .add("6", '1', ' ', L, "6")
            .run(listener);
        final String result = String.valueOf(listener.getResult()).trim();
        if (isPalindrome) {
            Assertions.assertEquals(result.trim(), "1");
        } else {
            Assertions.assertEquals(result.trim(), "0");
        }
    }

}
