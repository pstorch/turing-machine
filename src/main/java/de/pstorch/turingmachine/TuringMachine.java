package de.pstorch.turingmachine;

import java.util.HashMap;
import java.util.Map;

/**
 * Turing Machine which moves the head and expands the band as necessary.
 */
public class TuringMachine {

    private final Map<String, Instruction> instructions = new HashMap<>();

    private String endState = "H";
    private char defaultSymbol = ' ';
    private char[] band = new char[] { defaultSymbol };
    private String state = "S1";
    private int pos = 0;

    public TuringMachine() {
        super();
    }

    public TuringMachine startBand(final String startBand) {
        this.band = startBand.toCharArray();
        return this;
    }

    public TuringMachine startBand(final char[] startBand) {
        this.band = startBand;
        return this;
    }

    public TuringMachine startState(final String startState) {
        this.state = startState;
        return this;
    }

    public TuringMachine endState(final String endState) {
        this.endState = endState;
        return this;
    }

    public TuringMachine defaultSymbol(final char defaultSymbol) {
        this.defaultSymbol = defaultSymbol;
        return this;
    }

    public TuringMachine startPos(final int startPos) {
        this.pos = startPos;
        return this;
    }

    public TuringMachine add(final String state, final char symbol, final Character newSymbol, final Direction direction, final String newState) {
        instructions.put(state + symbol, new Instruction(state, symbol, newSymbol, direction, newState));
        return this;
    }

    public String run() {
        final DefaultStateListener listener = new DefaultStateListener();
        run(listener, -1);
        return String.valueOf(listener.getResult());
    }

    public void run(final StateListener listener) {
        run(listener, -1);
    }

    public void run(final StateListener listener, final int maxIterations) {
        int iteration = 0;
        while (!state.equals(endState) && (maxIterations == -1 || iteration < maxIterations)) {
            listener.update(iteration, state, pos, band);
            final Instruction instruction = instructions.get(state + band[pos]);
            if (instruction == null) {
                listener.instructionMissing(state, pos, band);
                break;
            }
            state = instruction.getNewState();
            final Character newCharacter = instruction.getNextSymbol();
            if (newCharacter != null) {
                band[pos] = newCharacter;
            }
            pos = instruction.getDirection().newPos(pos);

            if (pos < 0) {
                band = expandLeft();
                pos = 0;
            } else if (pos == band.length) {
                band = expandRight();
            }
            iteration++;
        }
        if (iteration == maxIterations) {
            listener.maxIterationsReached(iteration);
        }

        listener.result(iteration, state, pos, band);
    }

    private char[] expandRight() {
        char[] copy = new char[band.length + 1];
        System.arraycopy(band, 0, copy, 0, band.length);
        copy[band.length] = defaultSymbol;
        return copy;
    }

    private char[] expandLeft() {
        char[] copy = new char[band.length + 1];
        System.arraycopy(band, 0, copy, 1, band.length);
        copy[0] = defaultSymbol;
        return copy;
    }

}
