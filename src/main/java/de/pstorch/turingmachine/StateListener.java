package de.pstorch.turingmachine;

public interface StateListener {
    void update(final int iteration, final String state, final int pos, final char[] band);

    void instructionMissing(final String state, final int pos, final char[] band);

    void maxIterationsReached(final int iteration);

    void result(final int iteration, final String state, final int pos, final char[] band);
}
