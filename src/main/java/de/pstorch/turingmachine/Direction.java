package de.pstorch.turingmachine;

public enum Direction {

    L {
        @Override
        int newPos(final int oldPos) {
            return oldPos - 1;
        }
    },
    R {
        @Override
        int newPos(final int oldPos) {
            return oldPos + 1;
        }
    },
    N {
        @Override
        int newPos(int oldPos) {
            return oldPos;
        }
    };

    abstract int newPos(final int oldPos);

}
