package de.pstorch.turingmachine;

public class DefaultStateListener implements StateListener {

    private char[] result;
    private int iterations;
    private boolean maxIterationsReached;

    @Override
    public void update(final int iteration, final String state, final int pos, final char[] band) {
        System.out.println(String.format("%s[%d]: \"%s\"", state, pos, String.valueOf(band)));
    }

    @Override
    public void instructionMissing(final String state, final int pos, final char[] band) {
        System.out.println(String.format("No instruction found for state[%s] and symbol[%c]", state, band[pos]));
    }

    @Override
    public void maxIterationsReached(int iteration) {
        System.out.println("Max iterations reached: " + iteration);
        maxIterationsReached = true;
    }

    @Override
    public void result(int iteration, final String state, int pos, char[] band) {
        System.out.println(String.format("%s[%d]: \"%s\"", state, pos, String.valueOf(band)));
        System.out.println(String.format("Iterations: %d", iteration));
        result = band;
        iterations = iteration;
    }

    public char[] getResult() {
        return result;
    }

    public int getIterations() {
        return iterations;
    }

    public boolean isMaxIterationsReached() {
        return maxIterationsReached;
    }
}
