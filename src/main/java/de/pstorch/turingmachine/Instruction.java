package de.pstorch.turingmachine;

public class Instruction {

    private final String state;

    private final char symbol;

    private final String newState;

    private final Character nextSymbol;

    private final Direction direction;

    public Instruction(final String state, final char symbol, final Character nextSymbol, final Direction direction, final String newState) {
        super();
        this.state = state;
        this.symbol = symbol;
        this.newState = newState;
        this.nextSymbol = nextSymbol;
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Instruction instruction = (Instruction) o;

        if (symbol != instruction.symbol) return false;
        return state.equals(instruction.state);
    }

    @Override
    public int hashCode() {
        int result = state.hashCode();
        result = 31 * result + (int) symbol;
        return result;
    }

    public String getState() {
        return state;
    }

    public char getSymbol() {
        return symbol;
    }

    public String getNewState() {
        return newState;
    }

    public Character getNextSymbol() {
        return nextSymbol;
    }

    public Direction getDirection() {
        return direction;
    }

}
