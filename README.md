# Turing Machine

A simple Java implementation of a Turing Machine. It moves the head and expands the band as necessary.

## Build

Build project with maven `mvn clean install` or with a Java IDE.

## Usage
Several examples can be found in the Unittest.

1. Construct a `TuringMachine` instance with the starting band, start state, end state, default character on the band and start position of the head on the band:
```java
        TuringMachine machine = new TuringMachine(" ", "q1", "H", ' ', 0);
```

2. Add several functions to the state table, with the state name, the current symbol on the band, the symbol to write, the direction to move the head and the next state:
```java
        machine.addFunc("q1", ' ', '0', R, "q2");
        machine.addFunc("q2", ' ', ' ', R, "q3");
        machine.addFunc("q3", ' ', '1', R, "q4");
        machine.addFunc("q4", ' ', ' ', R, "q1");
```

If you have instructions which move the band instead of the head, just invert the direction.

3. Run the machine with tracing on/off and optionally a max interation counter to prevent endless loops:
```java
        final String result = machine.run(true, 21);
```
